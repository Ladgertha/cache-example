package ru.ladgertha.cacheexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_name.*

/**
 * @author Anna
 */
class NameActivity : AppCompatActivity() {
    private val dataSource = DataSource.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name)
        dataSource.getCache()?.apply {
            et_name.setText(this)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        dataSource.saveCache(et_name.text.toString())
    }
}