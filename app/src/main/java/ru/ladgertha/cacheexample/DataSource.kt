package ru.ladgertha.cacheexample

/**
 * @author Anna
 */

private const val NAME = "name"
class DataSource {

    companion object {
        val instance = DataSource()
    }

    private val cache = HashMap<String, String>()

    fun getCache(): String? {
        return if(cache.isNotEmpty()) cache[NAME]
        else null
    }

    fun saveCache(name: String?) {
        name?.apply {
            cache[NAME] = name
        }
    }
}